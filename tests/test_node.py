import unittest

from pynet import Encryptor, Node, PeerDefinition, NodeHooks
from pynet.util import to_bytes, _run_node, send_data


addr_1 = ("localhost", 54320)
# addr_1 = "/tmp/pynet_test_node.sock"
addr_2 = ("localhost", 54321)
node_pk = Encryptor.new_key()
master_encryptor = Encryptor(Encryptor.new_key(), node_pk.publickey())
master_def = PeerDefinition("master", None, master_encryptor.private_key.publickey())


def handle_message(sender, body):
  return (True, body.upper(), None)


class HooksOverride(NodeHooks):
  def handle_raw_message(self, data):
    if data == b"register":
      self.node.write("Registered!", encrypt=False)
      return (True, None)
    else:
      return super().handle_raw_message(data)


class TestNode(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    cls.addr = addr_1
    cls.node = Node(handle_message, cls.addr, "slave-1", node_pk, [master_def])
    cls.thread = _run_node(cls.node)

  @classmethod
  def tearDownClass(cls):
    cls.node.stop()
    cls.thread.join()

  def test_ping(self):
    response = send_data(self.addr, b"ping")
    self.assertEqual(response, b"pong")

  def test_identify(self):
    response = send_data(self.addr, b"identify")
    expected_pk = self.node.name.encode() + b"\n" + node_pk.publickey().exportKey("PEM")
    self.assertEqual(response, expected_pk)

  def log_bytes_stats(self, data):
    import logging
    print()
    logging.warning("Data length:    {0}".format(len(data)))
    logging.warning("First 10 bytes: {0}".format(data[:10]))
    logging.warning("Last 10 bytes:  {0}".format(data[-10:]))

    byted = {}
    for i, b in enumerate(data):
      if b not in byted:
        byted[b] = dict(count=0, indices=[])
      byted[b]["count"] += 1
      byted[b]["indices"].append(i)

    # for byte, stats in sorted(byted.items()):
    #   print("{0:4}: {1:3}; {2}".format(byte, stats["count"], ", ".join(str(i) for i in stats["indices"])))

  def test_message(self):
    send_message = b"hello there to you my good friends how is it that you are doing today I really would like to know oh yes I would"
    signed_cipher_text = Node.construct_message(master_encryptor, master_def.name, send_message)
    # self.log_bytes_stats(signed_cipher_text)
    response = send_data(self.addr, signed_cipher_text)
    message, signature = master_encryptor.decrypt(response)
    sender, status, body = message.split(b"\n", 2)
    self.assertEqual(sender, self.node.name.encode())
    self.assertEqual(status, b"ok")
    self.assertEqual(body, send_message.upper())
    self.assertTrue(master_encryptor.verify_message(message, signature))

  def test_get_config(self):
    cfg = self.node.get_config()
    self.assertEqual(set(cfg.keys()), {"address", "name", "private_key", "known_peers"})
    self.assertEqual(len(cfg["known_peers"]), 1)
    kp1 = cfg["known_peers"][0]
    self.assertEqual(set(kp1.keys()), {"name", "address", "public_key"})

  def test_from_config(self):
    cfg = self.node.get_config()
    cfg["address"] = ":".join(str(o) for o in addr_2)
    node = Node.from_config(handle_message, cfg)

  def test_special_handling(self):
    self.node.hooks = HooksOverride()
    response = send_data(self.addr, b"register")
    self.assertEqual(response, b"Registered!")
