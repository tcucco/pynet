import unittest

from Crypto.PublicKey import RSA
from pynet import Encryptor

from . import print_key_on_error


def new_key(size=2048):
  return RSA.generate(size)


class TestEncryptor(unittest.TestCase):
  def test_encrypt_decrypt(self):
    k1 = new_key()
    k2 = new_key()
    k3 = new_key()

    sender = Encryptor(k1, k2.publickey())
    receiver = Encryptor(k2, k1.publickey())

    message = b"In the Land of Mordor where the Shadows lie." \
      b"One Ring to rule them all, One Ring to find them,\n" \
      b"One Ring to bring them all and in the darkness bind them\n" \
      b"In the Land of Mordor where the Shadows lie.\n"

    with print_key_on_error(sender, receiver):
      cipher_text = sender.encrypt(message)
      decrypted_message, signature = receiver.decrypt(cipher_text)

    self.assertEqual(message, decrypted_message)

    with self.assertRaises(Exception):
      mangled_cipher_text = cipher_text[2:4] + cipher_text[:2] + cipher_text[4:]
      receiver.decrypt(mangled_cipher_text)

  def test_signature(self):
    k1 = new_key()
    k2 = new_key()
    k3 = new_key()

    sender = Encryptor(k1, k2.publickey())
    receiver = Encryptor(k2, k1.publickey())
    deceiver = Encryptor(k3, k2.publickey())

    message = b"Erised stra ehru oyt ube cafru oyt on wohsi"

    with print_key_on_error(sender, receiver, deceiver):
      cipher_text = sender.encrypt(message)

      decrypted_message, signature = receiver.decrypt(cipher_text)
      self.assertTrue(receiver.verify_message(decrypted_message, signature))

      deceiver_text = deceiver.encrypt(message)

      decrypted_message, signature = receiver.decrypt(deceiver_text)
      self.assertFalse(receiver.verify_message(decrypted_message, signature))

      _, real_encrypted_message, _ = receiver.split_cipher_text(cipher_text)
      deceiver_aes, _, deceiver_signature = receiver.split_cipher_text(deceiver_text)
      cipher_text = deceiver_aes + real_encrypted_message + deceiver_signature

      decrypted_message, signature = receiver.decrypt(cipher_text)
      self.assertFalse(receiver.verify_message(decrypted_message, signature))
