import sys

sys.path.insert(0, "..")

from Crypto.Cipher import AES
from Crypto import Random

def encrypt(message, key, iv=None, mode=AES.MODE_CFB):
  if iv is None:
    iv = Random.new().read(AES.block_size)
  sender = AES.new(key, mode, iv)
  return iv + sender.encrypt(message)


def decrypt(cipher_text, key, mode=AES.MODE_CFB):
  iv, encrypted_message = cipher_text[:AES.block_size], cipher_text[AES.block_size:]
  receiver = AES.new(key, mode, iv)
  return receiver.decrypt(encrypted_message)

message = sys.argv[1]
key = Random.new().read(32)
encrypted_message = encrypt(message, key)
print(encrypted_message)
decrypted_message = decrypt(encrypted_message, key)
print(decrypted_message)
