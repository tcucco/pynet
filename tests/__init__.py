import logging
import sys
from contextlib import contextmanager


@contextmanager
def print_key_on_error(*encryptors):
  try:
    yield encryptors
  except Exception as ex:
    for i, encryptor in enumerate(encryptors):
      logging.warning("Private Key {}:".format(i))
      logging.warning(encryptor.private_key.exportKey("PEM"))
    raise ex


from .test_encryptor import TestEncryptor
from .test_node import TestNode
