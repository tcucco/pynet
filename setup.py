import os
from setuptools import setup, find_packages

version = "0.4.5"

setup(
    author="Trey Cucco",
    author_email="fcucco@gmail.com",
    description="A generalalized worker network, in Python",
    download_url="https://gitlab.com/tcucco/pynet/-/archive/master/pynet-master.tar",
    install_requires=["pycrypto"],
    license="MIT",
    name="pynet",
    packages=find_packages(),
    url="https://gitlab.com/tcucco/pynet",
    version=version,
)
