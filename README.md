PyNet - A generalalized worker network, in Python.

The goal is to have nodes on seperate servers, or processes, that can communicate with each other
securely. The default implementation requires communicates to have their public keys registered
with each other before communication can happen, although it's possible to extend the functionality
through hooks to allow ad-hoc connecting.

I'm mostly doing this for learning, most if not all of what I'm hoping to
accomplish could probably be done easily with existing tools.
