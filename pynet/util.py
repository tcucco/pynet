import socket
import threading
from contextlib import contextmanager


def to_bytes(s):
  """If s is bytes, return s. If s is a string, return s.encode()."""
  if isinstance(s, bytes):
    return s
  if isinstance(s, str):
    return s.encode()
  raise ValueError("s must be a string or bytes")


def to_address(addr):
  """Convert addr into an address that can be used for a socket. It can be one of:
  - a 2-tuple of (ip, port). This value will be returned unchanged
  - a string of ip:port. This will be assumed if ':' is found in the string. A 2-tuple of (ip, port)
    will be returned.
  - Any other string. This will be returned unchanged to be used as a unix socket endpoint.
  """
  if isinstance(addr, str):
    if ":" in addr:
      ip, port = addr.split(":")
      return (ip, int(port))
    else:
      return addr
  if isinstance(addr, tuple):
    return addr
  raise ValueError("addr must be a string or tuple")


def _run_node(node):
  thread = threading.Thread(target=node.start)
  thread.start()
  return thread


@contextmanager
def run_node(node):
  thread = _run_node(node)
  try:
    yield node
  finally:
    node.stop()
    thread.join()


def send_data(send_to, data, size=4096):
  if isinstance(send_to, tuple):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  else:
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
  sock.connect(send_to)
  try:
    sock.sendall(to_bytes(data))
    buf = b""
    while True:
      data = sock.recv(size)
      if not data:
        break
      full = len(data) == size
      buf = buf + data
      if not full:
        break
    return buf
  finally:
    sock.close()


# TODO: I may start doing content length as the first 4 bytes of the messages, in which case I'll
#       need these...
def int_to_bytes(int_):
  return ((int_ & 0xff000000) >> 24, (int_ & 0xff0000) >> 16, (int_ & 0xff00) >> 8, int_ & 0xff)


def int_from_bytes(btup):
  return (btup[0] << 24) | (btup[1] << 16) | (btup[2] << 8) | btup[3]
