import os
import select
import socketserver
import uuid
from enum import Enum

from . import Encryptor
from .util import to_bytes, to_address


socketserver.TCPServer.allow_reuse_address = True
socketserver.UnixStreamServer.allow_reuse_address = True
RECV_SIZE = 8192


class Handler(socketserver.BaseRequestHandler):
  """A socketserver.BaseRequestHandler specialization, accepting a handle_data method from the
  caller.
  """
  def __init__(self, handle_data, *args):
    """Initialize the socket handler class.

    handle_data: a function accepting two arguments: bytes, socket
    The return value from handle_data should be either None, -1 or a non-negative number, these
    values are interpreted as follows:
      None: close the connection
      -1: Wait indefinitely for a response
      0+: The number of seconds to wait for a response

    The remaining arguments are passed to BaseRequestHandler.__init__
    """
    self.handle_data = handle_data
    super().__init__(*args)

  def has_data(self, wait=0):
    """Checks the request socket to see if there is any data waiting to be read."""
    return bool(select.select([self.request], [], [], wait)[0])

  def read_data(self):
    """Reads a RECV_SIZE chunk of data from the socket."""
    return self.request.recv(RECV_SIZE)

  def read_all_data(self, first_wait=0):
    """Returns all data from the request socket."""
    buff = b""
    while True:
      if not self.has_data(first_wait):
        break
      first_wait = 0
      data = self.read_data()
      buff += data
      if len(data) < RECV_SIZE:
        break
    return buff

  def handle(self):
    """Calls the handle_data method in a loop, continuing to wait for data depending on the return
    value of hanld_data.
    """
    keep_running = 0.1
    while True:
      data = self.read_all_data(keep_running)
      keep_running = self.handle_data(data, self.request)
      if keep_running is None:
        break
      elif keep_running == -1:
        keep_running = None


class PeerDefinition(object):
  """Contains the information known about a peer node: name, address and public_key."""
  __slots__ = ("name", "address", "public_key")

  def __init__(self, name, address, public_key):
    self.name = name
    self.address = address
    self.public_key = public_key

  def to_dict(self):
    if isinstance(self.public_key, str):
      pub_key = self.public_key
    elif isinstance(self.public_key, bytes):
      pub_key = self.public_key.decode("UTF-8")
    else:
      pub_key = self.public_key.exportKey("PEM").decode("UTF-8")

    return dict(
      name=self.name,
      address=self.address,
      public_key=pub_key)


class NodeHooks(object):
  """Subclass this and pass an instance to a node object to hook into the message cycle."""
  def __init__(self):
    self.node = None

  def handle_no_data_received(self):
    self.node.write("no data received", encrypt=False)
    return (True, None)

  def handle_raw_message(self, data):
    if data == b"ping":
      self.node.write("pong", encrypt=False)
      return (True, None)
    elif data == b"identify":
      self.node.write(self.node.name.encode() + b"\n" + self.node.public_key, encrypt=False)
      return (True, None)
    return (False, None)

  def handle_invalid_data(self, data):
    return (False, None)

  def handle_invalid_message_format(self, message, signature):
    return (False, None)

  def handle_unknown_peer(self, sender, body, signature):
    return (False, None)

  def handle_invalid_signature(self, sender, body, signature):
    return (False, None)


class MessageState(Enum):
  ok = 1
  invalid_data = 2
  invalid_message_format = 3
  unknown_peer = 4
  invalid_signature = 5


class Node(object):
  """A node in the pynet."""
  def __init__(self, handle_message, address, name=None, private_key=None, known_peers=[], hooks=None):
    """Initializes the node.

    Parameters:
    name - the name this node will identify itself with
    address - a 2-tuple of (ip, port) the socketserver will bind to, or a string in one of two
              formats: either "ip:port" or a file path, which will result in the socketserver using
              a UNIX socket.
    handle_message - a function that accepts a sender string and a bytes object as input and returns
                     a 3-tuple of:
                       (bool|None is_ok, str|bytes|None response_data, float|None wait_for_response)
                     These return values are interpreted as follows:
                     is_ok - if None, no response is sent to sender. if True an 'ok' response is
                             sent, if False an 'error' response is sent.
                     response_data - if None, only the is_ok portion of the response is sent,
                                     otherwise, the response_data is sent along with the is_ok flag.
                     wait_for_response - if None, the connection is closed. If -1 the server will
                                         wait indefinitely for a response, otherwise the server will
                                         wait wait_for_response seconds for a response.
    private_key - either a str or bytes in the PEM format, or a RSA key object.
    known_peers - a list of PeerDefinitions from which the node will accept messages.
    hooks - an instance of the NodeHooks or subclass
    """
    self.handle_message = handle_message
    self.address = to_address(address)
    self.name = name or str(uuid.uuid4())
    self.encryptor = Encryptor(private_key or Encryptor.new_key())
    self.known_peers = {}
    self.hooks = hooks or NodeHooks()
    self._server = None
    self.wsocket = None

    self.hooks.node = self

    for peer in known_peers:
      self.add_peer(peer)

  @property
  def public_key(self):
    """Returns the public key half of this node's private key in the PEM format."""
    return self.encryptor.private_key.publickey().exportKey("PEM")

  @property
  def server(self):
    if self._server is None:
      self._server = self._get_server()
    return self._server

  @property
  def hooks(self):
    return self._hooks

  @hooks.setter
  def hooks(self, value):
    self._hooks = value
    self._hooks.node = self

  @classmethod
  def from_config(cls, handle_message, config, hooks=None):
    """Creates a new Node instance from a config dictionary, as dumped out by get_config."""
    cfg = dict(config)
    cfg["known_peers"] = [PeerDefinition(**kp) for kp in cfg["known_peers"]]
    return cls(handle_message, hooks=hooks, **cfg)

  @classmethod
  def build_message(cls, sender_id, body):
    return to_bytes(sender_id) + b"\n" + to_bytes(body)

  @classmethod
  def construct_message(cls, encryptor, sender_id, body):
    """Constructs, encrypts and signs a message.

    Nodes expect messages to be in a particular format:
    cipher_text + signature
    where cipher_text is:
      sender_id + \\n + body

    This method takes a message body, sender id and encryptor and creates a message in this format.
    """
    message = cls.build_message(sender_id, body)
    return encryptor.encrypt(message)

  @classmethod
  def deconstruct_message(cls, encryptor, data):
    try:
      message, signature = encryptor.decrypt(data)
    except:
      return (MessageState.invalid_data, data)

    message_parts = message.split(b"\n", 1)
    if len(message_parts) != 2:
      return (MessageState.invalid_message_format, (message, signature))

    sender, body = message_parts
    sender = sender.decode("UTF-8")

    return (MessageState.ok, (sender, body, signature))

  @classmethod
  def verify_message(cls, encryptor, sender_id, body, signature):
    message = cls.build_message(sender_id, body)
    return encryptor.verify_message(message, signature)

  def _get_server(self):
    if isinstance(self.address, tuple):
      return socketserver.TCPServer(self.address, self.get_handler)
    else:
      return socketserver.UnixStreamServer(self.address, self.get_handler)

  def get_config(self):
    """Returns configuration data that can be used to create a new instance like this Node, via the
    from_config method.
    """
    return dict(
      address=":".join(str(o) for o in self.address) if isinstance(self.address, tuple) else self.address,
      name=self.name,
      private_key=self.encryptor.private_key.exportKey("PEM").decode("UTF-8"),
      known_peers=[p.to_dict() for p in self.known_peers.values()])

  def has_peer(self, name):
    return name in self.known_peers

  def add_peer(self, peer):
    """Adds a peer to the known peers directory."""
    if self.has_peer(peer.name):
      raise Exception("Duplicate peer: {0}".format(peer.name))
    self.known_peers[peer.name] = peer

  def remove_peer(self, name):
    del self.known_peers[name]

  def get_handler(self, *args):
    """Returns a handler object for the socketserver."""
    return Handler(self.handle_data, *args)

  def start(self):
    """Start the socketserver."""
    self.server.serve_forever()

  def stop(self):
    """Shutdown the socketserver and remove the socket file (if any)."""
    self.server.shutdown()
    if isinstance(self.address, str) and os.path.exists(self.address):
      os.remove(self.address)

  def handle_data(self, data, wsocket):
    """This method is passed to the handler object that the socketserver uses.

    It will automatically respond to 'ping' and 'identify' messages. All other messages will be
    decoded, verified and passed to the handle_message method provided to the constructor.

    The response of the handle_message method is fully handled in this method. See handle_message
    details on __init__ for more information.
    """
    self.wsocket = wsocket

    if len(data) == 0:
      handled, wait_for_response = self.hooks.handle_no_data_received()
      if handled:
        return wait_for_response
      else:
        return None
    else:
      handled, wait_for_response = self.hooks.handle_raw_message(data)
      if handled:
        return wait_for_response
      else:
        message_state, response_data = self.decode(data)

        if message_state == MessageState.invalid_data:
          handled, wait_for_response = self.hooks.handle_invalid_data(response_data)
          if handled:
            return wait_for_response
          else:
            self.write_error("Bad data", encrypt=False)
            return None

        elif message_state == MessageState.invalid_message_format:
          message, signature = response_data
          handled, wait_for_response = self.hooks.handle_invalid_message_format(message, signature)
          if handled:
            return wait_for_response
          else:
            self.write_error("Invalid message format", encrypt=False)
            return None

        elif message_state == MessageState.unknown_peer:
          sender, body, signature = response_data
          handled, wait_for_response = self.hooks.handle_unknown_peer(sender, body, signature)
          if handled:
            return wait_for_response
          else:
            self.write_error("Unknown peer", encrypt=False)

        elif message_state == MessageState.invalid_signature:
          sender, body, signature = response_data
          handled, wait_for_response = self.hooks.handle_invalid_signature(sender, body, signature)
          if handled:
            return wait_for_response
          else:
            self.write_error("Invalid signature", encrypt=False)

        elif message_state == MessageState.ok:
          sender, body, signature = response_data
          is_ok, response_data, wait_for_response = self.handle_message(sender, body)
          if is_ok is not None:
            self.write_response(is_ok, response_data)
          return wait_for_response

        else:
          raise Exception("Unknown message state")

  def set_receiver(self, peer):
    self.encryptor.public_key = peer.public_key

  def decode(self, data):
    """Decodes a message received by the socketserver."""
    # 1. use encryptor to decode message
    # 2. get sender id
    # 3. verify signer from sender's public key
    # 4. return decoded data
    # TODO: Write out expected message format?
    message_state, response_data = self.deconstruct_message(self.encryptor, data)

    if message_state != MessageState.ok:
      return (message_state, response_data)
    else:
      sender, body, signature = response_data

      if sender not in self.known_peers:
        return (MessageState.unknown_peer, (sender, body, signature))

      self.set_receiver(self.known_peers[sender])
      if not self.verify_message(self.encryptor, sender, body, signature):
        return (MessageState.invalid_signature, (sender, body, signature))

      return (MessageState.ok, (sender, body, signature))

  def _write(self, data):
    """Writes data to the current socket."""
    self.wsocket.sendall(to_bytes(data))

  def write(self, data, *, encrypt=True):
    """Writes data to the current socket.

    Parameters:
    encrypt - if True (the default) the data will be constructed into a message using the
              construct_message method.
    """
    if encrypt:
      self._write(self.construct_message(self.encryptor, self.name, data))
    else:
      self._write(data)

  def write_error(self, data=None, *, encrypt=True):
    """Creates an error response with the given data and sends it to the write method.

    Parameters:
    data - a bytes object or string to be sent over the socket.
    """
    if data is None:
      self.write(b"error", encrypt=encrypt)
    elif isinstance(data, str):
      self.write("error\n" + data, encrypt=encrypt)
    else:
      self.write(b"error\n" + data, encrypt=encrypt)

  def write_success(self, data=None, *, encrypt=True):
    """Creates a success response with the given data and sends it to the write method.

    Parameters:
    data - a bytes object or string to be sent over the socket.
    """
    if data is None:
      self.write(b"ok", encrypt=encrypt)
    elif isinstance(data, str):
      self.write("ok\n" + data, encrypt=encrypt)
    else:
      self.write(b"ok\n" + data, encrypt=encrypt)

  def write_response(self, is_ok, data=None, *, encrypt=True):
    """Calls either write_success or write_error depending on the value of is_ok.

    Parameters:
    is_ok - boolean value. When True write_success is called with data. When False, write_error.
    data - the data to be sent to either write_error or write_success.
    """
    if is_ok:
      self.write_success(data, encrypt=encrypt)
    else:
      self.write_error(data, encrypt=encrypt)
